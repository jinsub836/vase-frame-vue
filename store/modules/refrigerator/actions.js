import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.GET_REFRIGERATOR_LIST] :(store) =>{
        return axios.get(apiUrls.GET_REFRIGERATOR_LIST)
    },
}
