const BASE_URL = 'http://api.myrefrigerator.shop:8080/v1/refrigerator/'

export default {
    GET_REFRIGERATOR_LIST:`${BASE_URL}/all`
}
