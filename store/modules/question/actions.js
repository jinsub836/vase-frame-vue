import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'
import {store} from "core-js/internals/reflect-metadata";

export default {
    [Constants.GET_QUESTION_LIST] :(store) =>{
        return axios.get(apiUrls.GET_QUESTION_LIST)
    },
    [Constants.GET_QUESTION_DETAIL]: (store,payload) => {
        return axios.get(apiUrls.GET_QUESTION_DETAIL.replace('{id}', payload.id))
    },
    [Constants.DO_ANSWER]: (store, payload) =>{
        return axios.post(apiUrls.DO_ANSWER.replace('{id}',payload.id) ,payload.data)
    }


   // [Constants.GET_MEMBER_DETAIL]: (store,payload) => {
    //    return axios.get(apiUrls.GET_MEMBER_DETAIL.replace('{id}', payload.id))
   // },
    //[Constants.DO_CORRECT_MEMBER_DETAIL]: (store, payload) => {
   //     return axios.put(apiUrls.DO_CORRECT_MEMBER_DETAIL.replace('{id}', payload.id), payload.data)
   // },[Constants.DO_CORRECT_MEMBER_DETAIL]: (store, payload) => {
    //      return axios.put(apiUrls.DO_CORRECT_MEMBER_DETAIL.replace('{id}', payload.id), payload.data)
    //     },
}
