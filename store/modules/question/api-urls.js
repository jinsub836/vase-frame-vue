const BASE_URL = 'http://api.myrefrigerator.shop:8080/v1/questions'
const BASE_URL2 = 'http://api.myrefrigerator.shop:8080/v1/answer'


export default {
    GET_QUESTION_LIST: `${BASE_URL}/all`,
    GET_QUESTION_DETAIL:`${BASE_URL}/detail/{id}`,
    DO_ANSWER:`${BASE_URL2}/new/{id}`
}
