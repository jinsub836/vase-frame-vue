export default {
    GET_QUESTION_LIST: 'question/list',
    GET_QUESTION_DETAIL: 'question/detail',
    DO_ANSWER:'answer/add'
}
