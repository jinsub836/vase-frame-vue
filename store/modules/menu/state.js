const state = () => ({
    globalMenu: [
        {
            parentName: '대시 보드',
            icon: 'el-icon-tickets',
            menuLabel: [
                { id: 'DASH_BOARD', currentName: '대시 보드', link: '/', isShow: true },
            ]
        },
        {
            parentName: '회원관리',
            icon: 'el-icon-tickets',
            menuLabel: [
                { id: 'MEMBER_ADD', currentName: '회원 등록', link: '/member/create', isShow: true },
                { id: 'MEMBER_LIST', currentName: '회원 리스트', link: '/member/list2', isShow: true }, // 연동 O
                { id: 'MEMBER_DETAIL', currentName: '회원 상세 정보', link: '/member/detail', isShow: true }, //연동 O
                { id: 'MEMBER_DETAIL_CORRECTION', currentName: '회원 정보 수정', link: '/member/detail-correction', isShow: true }, //연동 X
            ]
        },
        {
            parentName: '냉장고 관리',
            icon: 'el-icon-tickets',
            menuLabel: [
                { id: 'REFRIGERATOR_INPUT', currentName: '냉장고 재료 등록', link: '/refrigerator/input', isShow: true },
                { id: 'REFRIGERATOR_LIST', currentName: '냉장고 재료 목록', link: '/refrigerator/list', isShow: true },
            ]
        },
        {
            parentName: '레시피관리',
            icon: 'el-icon-tickets',
            menuLabel: [
                { id: 'RECIPE_FOOD-INPUT', currentName: '레시피 식재료 등록', link: '/recipe/food-input', isShow: true }, //연동 완료
                { id: 'RECIPE_FOOD_LIST', currentName: '레시피 식재료 목록', link: '/recipe/food/add', isShow: true },
                { id: 'RECIPE_ADD', currentName: '레시피 등록', link: '/recipe/recipe-add', isShow: true }, //
                { id: 'RECIPE_LIST', currentName: '레시피 목록', link: '/recipe/list', isShow: true }, // 만들어야됨
                { id: 'RECIPE_DETAIL', currentName: '레시피 상세보기', link: '/recipe/detail', isShow: true },
                { id: 'RECIPE_CORRECTION', currentName: '레시피 수정', link: '/recipe/correction', isShow: true },//만들어야됨
                { id: 'RECIPE_DELL', currentName: '레시피 삭제', link: '/recipe/correction', isShow: true },
            ],

        },
        {
            parentName: '문의 사항',
            icon: 'el-icon-tickets',
            menuLabel: [
                { id: 'QUESTION_LIST', currentName: '문의사항 목록', link: '/question/list2', isShow: true },
                { id: 'ANSWER_ADD', currentName: '문의사항 답변 등록', link: '/answer/add', isShow: true },
                { id: 'ANSWER_LIST', currentName: '문의사항 답변 목록', link: '/answer/list', isShow: true },
                { id: 'ANSWER_DETAIL', currentName: '문의사항 답변 상세보기', link: '/answer/detail', isShow: true },
                { id: 'ANSWER_CORRECTION', currentName: '문의사항 답변 수정하기', link: '/answer/correction', isShow: true },
            ],

        },
        {
            parentName: '광고 관리',
            icon: 'el-icon-tickets',
            menuLabel: [
                { id: 'COMMERCIAL_REGISTRATION', currentName: '광고 정보 등록', link: '/commercial/registration', isShow: true },
                { id: 'COMMERCIAL_LIST', currentName: '광고 리스트', link: '/commercial/list', isShow: true },
                { id: 'COMMERCIAL_DETAIL', currentName: '광고 정보 상세보기', link: '/commercial/detail', isShow: true },
                { id: 'COMMERCIAL_CORRECTION', currentName: '광고 정보 등록 수정', link: '/commercial/correction', isShow: true },
                { id: 'COMMERCIAL_TIMEOUT', currentName: '광고 정보 만료 목록', link: '/my-menu/logout', isShow: true },
            ],
        }
    ],
    selectedMenu: ''
})

export default state
