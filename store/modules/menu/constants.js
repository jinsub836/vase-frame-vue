export default {
    GET_MENUS_LIST: 'menu/getMenusList',
    FETCH_SELECTED_MENU: 'menu/fetchSelectedMenu',
    GET_SELECTED_MENU: 'menu/getSelectedMenu',
    DO_MENUS_LIST_NO_SHOW:'menu/getSelectMenu true'
}
