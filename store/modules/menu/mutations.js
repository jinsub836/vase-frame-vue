import Constants from './constants'
import state from './state'

export default {
    [Constants.FETCH_SELECTED_MENU]: (state, payload) => {
        console.log(payload)
        state.selectedMenu = payload
    },
    [Constants.DO_MENUS_LIST_NO_SHOW]: (state) => {
        state.isShow = true
    }
}
