const BASE_URL = 'v1/'

export default {
    DO_CREATE_COMMERCIAL: 'http://api.myrefrigerator.shop:8080/v1/Advertising/Payment', //post
    GET_COMMERCIAL_LIST:`http://api.myrefrigerator.shop:8080/v1/Advertising/all/{pageNum}`, //get
}
