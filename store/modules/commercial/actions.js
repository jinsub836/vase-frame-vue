import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    //[Constants.DO_CORRECT_MEMBER_DETAIL]: (store, payload) => {
     //   return axios.put(apiUrls.DO_CORRECT_MEMBER_DETAIL.replace('{id}', payload.id), payload.data)
   // },
    [Constants.DO_CREATE_COMMERCIAL] :(store, payload) =>{
        return axios.post(apiUrls.DO_CREATE_COMMERCIAL, payload.data)
    },
    [Constants.GET_COMMERCIAL_LIST] :(store,payload) =>{
      return axios.get(apiUrls.GET_COMMERCIAL_LIST.replace('{pageNum}',payload.pageNum))
    },

}
