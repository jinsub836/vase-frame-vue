export default {
   DO_ADD_FOOD_RECIPE:'recipe/food-input',
   GET_FOOD_RECIPE_LIST:'recipe/food/_id',
   GET_RECIPE_LIST:'recipe/food/detail',
   GET_RECIPE_DETAIL:'recipe/food/detail1',
   GET_RECIPE_NAME:'recipe/food/detail2',
   GET_RECIPE_FOOD:'recipe/food/detail3'
 //get 레시피 디테일
}
