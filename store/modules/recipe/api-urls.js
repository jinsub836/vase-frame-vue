const BASE_URL ='http://api.myrefrigerator.shop:8080/v1/refrigerator_food'
const BASE_URL2 ='http://api.myrefrigerator.shop:8080/v1/food-name'
const BASE_URL3 ='http://api.myrefrigerator.shop:8080/v1/order-of-cooking'


export default {
    DO_ADD_FOOD_RECIPE:`${BASE_URL}/new`, // post
    GET_FOOD_RECIPE_LIST:`${BASE_URL}/all/{pageNum}`, //get
    GET_RECIPE_LIST:`${BASE_URL2}/all`, //get 레시피 리스트
    GET_RECIPE_DETAIL:`${BASE_URL3}/all/{id}` ,
    GET_RECIPE_NAME:`${BASE_URL2}/detail/{id}`,
    GET_RECIPE_FOOD:'http://api.myrefrigerator.shop:8080/v1/food-ingredient/food-name-id/{foodNameId}'
}
