import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'
import {store} from "core-js/internals/reflect-metadata";

export default {
    [Constants.DO_ADD_FOOD_RECIPE] :(store, payload) =>{
        return axios.post(apiUrls.DO_ADD_FOOD_RECIPE, payload.data)
    },
    [Constants.GET_FOOD_RECIPE_LIST]: (store, payload) => {
        return axios.get(apiUrls.GET_FOOD_RECIPE_LIST.replace('{pageNum}', payload.pageNum))
    },
    [Constants.GET_RECIPE_LIST]: (store) => {
        return axios.get(apiUrls.GET_RECIPE_LIST)
    },
    [Constants.GET_RECIPE_DETAIL] : (store , payload) => {
        return axios.get(apiUrls.GET_RECIPE_DETAIL.replace('{id}', payload.id))
    },
    [Constants.GET_RECIPE_NAME] : (store , payload) => {
        return axios.get(apiUrls.GET_RECIPE_NAME.replace('{id}',payload.id))
    },
    [Constants.GET_RECIPE_FOOD] : (store , payload) => {
        return axios.get(apiUrls.GET_RECIPE_FOOD.replace('{foodNameId}',payload.id))
    }
}
