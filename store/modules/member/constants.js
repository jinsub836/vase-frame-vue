export default {
    DO_MEMBER_ADD:'member/add',
    GET_MEMBER_LIST: 'member/all',
    GET_MEMBER_DETAIL:`member/detail/{id}`,
    DO_CORRECT_MEMBER_DETAIL:'member/out/',
    GET_ADMIN_ANSWER_LIST:'answer/all'
}
