const BASE_URL = 'http://api.myrefrigerator.shop:8080/v1/member'

export default {
    DO_MEMBER_ADD:'/new',
    GET_MEMBER_LIST: `${BASE_URL}/all`, //get
    GET_ADMIN_ANSWER_LIST:'http://api.myrefrigerator.shop:8080/v1/answer/all',
    GET_MEMBER_DETAIL:`${BASE_URL}/detail/{id}`,//get
    DO_CORRECT_MEMBER_DETAIL: `${BASE_URL}/status/{id}` //put

}
