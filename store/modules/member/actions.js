import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_MEMBER_ADD] :(store, payload) =>{
        return axios.post(apiUrls.DO_MEMBER_ADD, payload.data)
    },
    [Constants.GET_MEMBER_LIST] :(store) =>{
      return axios.get(apiUrls.GET_MEMBER_LIST)
    },
    [Constants.GET_ADMIN_ANSWER_LIST] :(store) =>{
        return axios.get(apiUrls.GET_ADMIN_ANSWER_LIST)
    },
    [Constants.GET_MEMBER_DETAIL]: (store,payload) => {
        return axios.get(apiUrls.GET_MEMBER_DETAIL.replace('{id}', payload.id))
    },
    [Constants.DO_CORRECT_MEMBER_DETAIL]: (store, payload) => {
        return axios.put(apiUrls.DO_CORRECT_MEMBER_DETAIL.replace('{id}', payload.id), payload.data)
    },
}
