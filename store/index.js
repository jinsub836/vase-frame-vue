import customLoading from './modules/custom-loading'
import authenticated from './modules/authenticated'
import menu from './modules/menu'
import testData from './modules/test-data'
import member from './modules/member'
import recipe from './modules/recipe'
import question from './modules/question'
import refrigerator from  './modules/refrigerator'
import commercial from './modules/commercial'

export const state = () => ({})

export const mutations = {}

export const modules = {
    customLoading,
    authenticated,
    menu,
    testData,
    member,
    recipe,
    question,
    refrigerator,
    commercial
}
