import Vue from 'vue'

import customLoadingConstants from '~/store/modules/custom-loading/constants'
Vue.prototype.$customLoadingConstants = customLoadingConstants

import authenticatedConstants from '~/store/modules/authenticated/constants'
Vue.prototype.$authenticatedConstants = authenticatedConstants

import menuConstants from '~/store/modules/menu/constants'
Vue.prototype.$menuConstants = menuConstants

import testDataConstants from '~/store/modules/test-data/constants'
Vue.prototype.$testDataConstants = testDataConstants

import memberConstants from '~/store/modules/member/constants'
Vue.prototype.$memberConstants = memberConstants

import recipeConstants from '~/store/modules/recipe/constants'
Vue.prototype.$recipeConstants = recipeConstants

import questionConstants from '~/store/modules/question/constants'
Vue.prototype.$questionConstants = questionConstants

import refrigeratorConstants from '~/store/modules/refrigerator/constants'
Vue.prototype.$refrigeratorConstants = refrigeratorConstants

import commercialConstants from '~/store/modules/commercial/constants'
Vue.prototype.commercialConstants = commercialConstants
